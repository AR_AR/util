#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  6 16:27:50 2018

@author: andrea
"""

import numpy as np
import matplotlib
#matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.dates as mdate

from matplotlib.colors import LogNorm
from natsort import natsorted
from scipy import ndimage
import glob, os
from natsort import natsorted
from scipy import stats
import scipy as sp
import time
import datetime
import pylab as pl
import pytz

#import utilities3 as ut3
import util.utilities3 as ut3


class MamboRead:
    '''Meant to read the mambo save files from SixS beamline.
    It replace the missing data marked as "*" in the txt file with NaN
    it also provide a simple plotting function:'''    
    def __init__(self,directory,workfile):
        F = open(directory + workfile ,'rt')
        self.line0  = F.readline()
        self.headers = self.line0.replace('-','_').split(';')
        self.fulldata = F.read().split('\n')
        self.epoch = []
        self.dates = []
        self._fields = len(self.line0.split('\t'))
        self.colNames = self.headers[0].split('\t')
        self.NameVsCol = {}
        self._tmz=pytz.timezone('Europe/Amsterdam')
        self._dataAttList = list()
        self._dataAttList.append('epoch')
        F.close()
       
        for col in np.arange(1,self._fields):
                self.__setattr__('col'+str(col), [])
                self._dataAttList.append('col'+str(col))
        
        for el in self.fulldata[:-1]:
            toto = el.split('\t')
            self.dates.append(toto[0])
            epoctime = time.mktime(datetime.datetime.strptime(toto[0][:-4], "%Y/%m/%d %H:%M:%S").timetuple())
            self.epoch.append(epoctime)
        
            for col in np.arange(1,self._fields):
                att = 'col'+str(col)
                self.NameVsCol.update({self.colNames[col]:att})
                self.__dict__[att].append(self.star2Nan(toto[col]))
        
        for el in self._dataAttList:
            self.__dict__[el] = np.asanyarray(self.__dict__[el])
            
        
        self.pltTime = self.epoch2pltTime(self.epoch)
    
    
    def epoch2pltTime(self, epoch):
         pltTime = []
         for el in epoch:
             toto = time.gmtime(el)
             pltTime.append (datetime.datetime(toto[0], toto[1], toto[2], toto[3], toto[4], toto[5], toto[6]))
         return pltTime             
                
        
        
        
    def star2Nan(self, value):
        if '*' in value:
            return np.NaN
        else:
            return float(value)
        
    def plotDateVar(self,ydata_strs):
        '''epoch vector, ['ydata1', 'ydata2', ... 'max 7 data'] '''
        colorvalues = ['r','g','b','k','y','c','m']
        secs = mdate.epoch2num(np.asanyarray(self.epoc)+7200) #7200 to correct for time zone and summer time
        fig,ax = plt.subplots()
    #for count, value in enumerate(values)
    # Plot the date using plot_date rather than plot
        for pos, yel in enumerate(ydata_strs):
            clr = colorvalues[pos]
            for el in self.NameVsCol.keys():
                if yel in el:
                    #print (self.NameVsCol[el])
                    toto = self.NameVsCol[el]
                    ydata = self.__getattribute__(toto)
                    ax.plot_date(secs,ydata, mfc = clr, mec =clr, label = yel)
    
    # Choose your xtick format string
        date_fmt = '%d-%m-%y %H:%M:%S'
    
    # Use a DateFormatter to set the data to the correct format.
        date_formatter = mdate.DateFormatter(date_fmt)
        ax.xaxis.set_major_formatter(date_formatter)
    
    # Sets the tick labels diagonal so they fit easier.
        fig.autofmt_xdate()
        locator = mdate.HourLocator( interval=1)#,tz=self._tmz) #not really working
        ax.xaxis.set_minor_locator(locator)
        plt.grid(b=True, which='major', color='b', linestyle='-')
        plt.grid(b=True, which='minor', color='r', linestyle='--')
        
        pl.legend(loc=0)

