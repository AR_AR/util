    #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 30 10:51:05 2019

@author: andrea
"""

'''To deal with rod output files, at least some of them.
There is also the rod.py in the mypy folder.
This was started to deal with the ROD simulation output '''


import scipy
import pylab as pl
import os
#import array
#import re
#import string
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import glob
import util.utilities3 as ut3


class RodLoad(object): #myfilename):
    def __init__(self, filename, directory = ''):
        self.directory = directory 
        self.filename = filename
        myfilename = os.path.join(self.directory,self.filename)
        self.data=np.genfromtxt(myfilename,dtype= 'float', delimiter='',skip_header = 2)
        infile = open(myfilename,'r')
        self.text = infile.read()
        infile.close()
        self.H = self.data[:,0]
        self.K = self.data[:,1]
        self.L = self.data[:,2]        
        self.Fsum = self.data[:,3]
        self.Phase = self.data[:,4]        
        
    def plotL(self, color, legend):
        plt.plot(self.L, self.Fsum, color, label = legend)
        plt.semilogy()
        plt.grid()
        plt.legend()
        
def returnFsum(directory, filename):
    myfilename = os.path.join(directory,filename)
    data=np.genfromtxt(myfilename,dtype= 'float', delimiter='',skip_header = 2)
    Fsum = data[:,3]
    L = data[:,2]
    return Fsum, L
       
def SelectFile(rootname, element, num, directory,splt = '_'):
    '''looks for the number at the end of the filename: rod10L_I_Termin_0.40 
    rootname: rod10L
    element :  I
    num     : 0.40'''
#    if directory == 'curr':
#        dirtotal = _recorder.projectdirectory + _recorder.subdirectory + '/'
#    else :
    dirtotal = directory
    li = glob.glob1(dirtotal, '*')
    for el in li:
        elRoot = el.split(splt)[0]
        elElement = el.split(splt)[1]
        elnumber = el.split(splt)[-1]
        
        if ut3.isFloat(elnumber) and elElement == element and elRoot == rootname:
            if elnumber == num:
                return  el            

def SelectFiles(rootname, element, directory,splt = '_'):
    '''looks for the number at the end of the filename: rod10L_I_Termin_0.40 
    rootname: rod10L
    element :  I
    num     : 0.40'''
#    if directory == 'curr':
#        dirtotal = _recorder.projectdirectory + _recorder.subdirectory + '/'
#    else :
    dirtotal = directory
    li = glob.glob1(dirtotal, '*')
    liSelect = []
    for el in li:
        elRoot = el.split(splt)[0]
        elElement = el.split(splt)[1]
        
        if elElement == element and elRoot == rootname:
            liSelect.append(el)
    return liSelect

def SelectNum(Shortlist, num, splt = '_'):
    '''To be applied on th eliste generated from SelectFiles '''
    for el in Shortlist:
        elnumber = el.split(splt)[-1]
        if ut3.isFloat(elnumber) and elnumber == num:
            return el
         
    
def isEven(num):
     if (num % 2) == 0:
         return True
     else:
         return False
     
def dat2hkList(DatFN):
    '''From the rod.dat file return a list of H K values'''
    f = open(DatFN, "rt")
    listout = [[np.pi, np.pi]]
    data = f.read()
    dln = data.splitlines()
    f.close()
    for el in dln[1:-1]: #skip first line
        lin = el.split()
        H = int(lin[0])
        K = int(lin[1])
        elList = [H, K]
        #print(elList)
        if (listout[-1]!=elList): # and listout[-1][1]!=elList[1]):
                #print(elList)
                listout.append(elList)
                #print(listout)
    #print(elList)
    return(listout[1:])

def dataSelectHK(datFN, Hd, Kd):
    '''Return L, Structure Factor, and error for H,K from the data file'''
    f = open(datFN, "rt")
    listout = [[np.pi, np.pi]]
    data = f.read()
    dln = data.splitlines()
    f.close()
    coords = [Hd,Kd]
    Lout=[]
    sfEout=[]
    sfOut =[]
    for el in dln[1:-1]: #skip first line
        lin = el.split()
        H = int(lin[0])
        K = int(lin[1])
        L = float(lin[2])
        sf = float(lin[3])
        sfE = float(lin[4])
        elList = [H, K]
        #print(elList)
        if (coords==elList): # and listout[-1][1]!=elList[1]):
            Lout.append(L)
            sfEout.append(sfE)
            sfOut.append(sf)
    #print(elList)
    return(Lout, sfEout, sfOut)
    
    
def RelBetaL(directory, Rootname, element, relative = 0):
    '''Select the rods to generate a matrix: relax vs beta sv L of the structure factors
    name structure expected to be: rod11L_I_Rel_470_beta_120.dat
    the d3[a, b, c] matrix is as follow 
    a dimension: L span
    b dimension: Beta span
    c dimension: Relax span
    relative is the reference fileNAME to get the difference map compare to the reference rod
    relative = 'rod11L_I_Rel_500_beta_000.dat' '''
    
    li = glob.glob1(directory, '*.dat')
    liSelect = []  
    for el in li:
        elsplit = el[:-4].split('_')
        if elsplit[0] == Rootname and elsplit[1] == element: # select the rod and termination
            liSelect.append(el)
    liSelect.sort()
    #get the starts/ends of relax and beta
    Rst = int(liSelect[0][:-4].split('_')[3])*0.01
    Bst = int(liSelect[0][:-4].split('_')[-1])*0.01
    Rnd = int(liSelect[-1][:-4].split('_')[3])*0.01
    Bnd = int(liSelect[-1][:-4].split('_')[-1])*0.01
    #return liSelect
    betas = [] #np.empty(0)
    relaxs = [] #np.empty(0)
    
    for pos, el in enumerate(liSelect): # get the relaxs and beta vectors from the names
        elsplit = el[:-4].split('_')
        if elsplit[3] not in relaxs:
            relaxs.append(elsplit[3])
        
        if elsplit[-1] not in betas:
            betas.append(elsplit[-1])
    betas = np.array(betas, dtype = float)*0.001
    relaxs = np.array(relaxs, dtype = float)*0.001
    ##### build the plane
    startname = liSelect[0][:-4].split('_')
    #print ('original ',startname)
    d3 = np.empty([1])
    for posR, elR in enumerate(relaxs):
        #print(elR)
        strRelax = '%.3f'%elR
        plane = np.empty([])
        for posB, elB in enumerate(betas):
            strBeta = '%.3f'%elB
            startname[3] = strRelax[2:]
            startname[-1] = strBeta[2:]
            filename = '_'.join(startname)+'.dat' ## building the filename to read
            #print(filename)
            if relative == 0:
                Fsum, L = returnFsum(directory, filename)
            if relative != 0:
                try:
                    Frel, L = returnFsum(directory, relative)
                    Floc, L = returnFsum(directory, filename)
                    Fsum = Floc/Frel
                except:
                    print('relative error')
                    
            #print (len(np.shape(Fsum)), ' Fsum shape')
            if len(np.shape(plane))==0:
                #print('less than 2')
                plane = Fsum
            elif  len(np.shape(plane)) >= 1:
                plane = np.c_[plane, Fsum]
                #print('more than 2')
                #print (np.shape(plane))
        if len(np.shape(d3)) < 2:
            d3 = plane
            #print (np.shape(plane))
            #print('less than 2')
            #np.expand_dims(d3, 2)
            #print(np.shape(d3))
        if len(np.shape(d3))>=2:
            #d3 = np.array([d3, plane])
            #d3 = np.append(d3, plane, axis=1)
            #d3 = np.r_[d3, plane]
            d3 = np.dstack([d3, plane])
            #d3 = np.concatenate([d3, plane], axis=2)
            #print(np.shape(d3))
            #            d3 = np.append(d3, plane, axis=0)
## plane relaxs build with: plane = np.c_[plane, rod]
## planes stucked with    : 3d = np.array([3d, plane])
    return L, betas, relaxs, d3    
            
def Lcut(Lvalue,L, betas, relaxs, d3, plot = 'YES', labels = 'NO'):
    '''Return one image in the betas/Relaxs plane at the given L '''
    Lpos, Lclosest = ut3.find_nearest(L, Lvalue)
    Limage = d3[Lpos, :, :]
    #print(np.shape(Limage))
    if plot == 'YES':
        plt.imshow(np.log10(Limage), cmap=plt.cm.jet, interpolation='none',origin='lower', extent=[min(relaxs),max(relaxs),min(betas),max(betas)], aspect = 'auto')
        #plt.set_aspect(1)
        if labels != 'NO':
            plt.xlabel('betas')
            plt.ylabel('relaxs')

        plt.colorbar()
    return Limage

def Betacut(Bvalue,L, betas, relaxs, d3, plot = 'YES', labels = 'NO'):
    '''Return one image in the betas/Relaxs plane at the given L '''
    Bpos, Bclosest = ut3.find_nearest(betas, Bvalue)
    Bimage = d3[:, Bpos, :]
    #print(np.shape(Bimage))
    if plot == 'YES':
        plt.imshow(np.log10(Bimage), cmap=plt.cm.jet, interpolation='none',origin='lower', extent=[min(relaxs),max(relaxs),min(L),max(L)], aspect = 'auto')
        plt.colorbar()
        if labels != 'NO':
            plt.xlabel('relaxs')
            plt.ylabel('L, (rlu)')

        #plt.set_aspect(1)
    return Bimage

def Relaxcut(Rvalue,L, betas, relaxs, d3, plot = 'YES', labels = 'NO'):
    '''Return one image in the betas/Relaxs plane at the given L '''
    Rpos, Rclosest = ut3.find_nearest(relaxs, Rvalue)
    Rimage = d3[:, :, Rpos]
    #print(np.shape(Rimage))
    if plot == 'YES':
        plt.imshow(np.log10(Rimage), cmap=plt.cm.jet, interpolation='none',origin='lower', extent=[min(betas),max(betas),min(L),max(L)], aspect = 'auto')
        plt.colorbar()
        if labels != 'NO':
            plt.xlabel('betas')
            plt.ylabel('L, (rlu)')
        #plt.set_aspect(1)
    return Rimage


def rodp(directory, rodFile, color, scaleMax = 'NO',label = None):
    '''Meant to plot the rods calculated by ROD.
    '''
    data=np.genfromtxt(directory + rodFile,dtype= 'float', delimiter='', skip_header=2)
    l = data[:,2]
    Fsum = data[:,6]
    if scaleMax != 'NO':
        maxpos = np.nanargmax(Fsum)
        print((maxpos, Fsum[maxpos]))
        Fsum = Fsum/Fsum[maxpos]       
    #data[:,1]=data[:,1]-min(data[:,1])
    if label == None:
        #plt.plot(L,Int,color,linewidth=2, label=rodFile[:-4])
        plt.plot(l,Fsum,color,marker='+',linewidth=0, label=rodFile[:-4])
    if (label !=None and label !='No'):
        #plt.plot(L,Int,color,linewidth=2, label=label)
        plt.plot(l,Fsum,color,marker='+',linewidth=0, label=label)
    if label =='No':
        #plt.plot(L,Int,color,linewidth=2)
        plt.plot(l,Fsum,color,marker='+',linewidth=0)
    
    pl.semilogy()
    pl.xlim(min(l),max(l))   
    #pl.xlim(0,2.2)
    pl.ylim(0, max(Fsum))
    #pl.ylim(0.4, max(data[:,1]))
    
    pl.yticks(fontsize=18)
    pl.xticks(fontsize=18)
    pl.xlabel('L(rlu)',fontsize=20)
    pl.ylabel('sf (a.u.)',fontsize=20)
    #leg = plt.legend(loc = 'best', numpoints=1, fancybox=True)
    #leg.get_frame().set_alpha(0.5)
    #plt.legend(bbox_to_anchor=(0., 1.04, 1., .102), loc=3,
    #       ncol=1, mode="expand", borderaxespad=0.)   
    plt.legend(loc=1,fontsize=14)
    plt.grid(True)
   # 
    plt.show()

        
#directory = '/home/andrea/SciProj/NH3ox/rod/sim04/'
#li = glob.glob1(directory,'*.dat')
#L, betas, relaxs, d3 = RelBetaL(directory, 'rod11L', 'Rh')#, relative = 'rod11L_I_Rel_500_beta_000.dat')
#Limage = Lcut(0.99, L, betas, relaxs, d3)
##Bimage = Betacut(0.1, L, betas, relaxs, d3)
##Rimage = Relaxcut(0.45, L, betas, relaxs, d3)
#np.savez(directory+'MatrixDumpRh', L, betas, relaxs, d3)
#fn = 'rod11L_I_Rel_450_beta_000.dat'
##Fsum, L = returnFsum(directory, 'rod11L_Rh_Rel_55_beta_49.dat')
#Rtest = RodLoad(fn, directory)
##Rtest.plotL('g', fn)
        