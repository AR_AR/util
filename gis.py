# -*- coding: utf-8 -*-
"""
Created on Mon Sep 29 16:47:54 2014

@author: andrea
"""
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)
import math
import numpy
#import xrayutilities as xutil
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import pylab as pl

''' small quick calculations for gisaxs
should improve with time'''

def en2nm(energy):
    '''return the wavelenght in nm a Q in nm^-1 '''
    waveL=1.239842/energy
    Q=2*numpy.pi/waveL
    return  waveL, Q#
def en2nmA(energy):
    '''return the wavelenght in A a Q in A^-1 '''
    waveL=12.39842/energy
    Q=2*numpy.pi/waveL
    return  waveL, Q
    
def nm2en(lam):
    '''wavelenght in nm, return the energy in keV. '''
    #waveL=1.239842/energy
    energy = 1.239842/lam
    #Q=2*numpy.pi/waveL
    return  energy
    

def ScAng(energy, spacing): 
    ''' given the energy and the expected sample feature size gives back 
    the angle expected for the gisaxs signal
    energy in keV and spacyng in nm'''
    
    ## keep in  mind  that in cx1 sample to detector ~1600mm
    ## xpad pixel size ~ 100microns ==> 0.0036degree
    ## beam stopper    ~ 1 mm wide  ==> 0.036 degree
    ## as role of thumb the resulting angle better be > 0.1deg (~2.7 mm at the detector)
    
    ## the XPAD detector is ~7.3x12.5 mm ==> 2.6x4.46 degree @1600mm
    
    ##convert the energy from keV to nm
    ## 1 nm =1239.84187 eV
    
# Q = 2pi/waveL    
# Qpar signal expected at  @ 2pi/d
# d = 2pi/Qpar
    waveL=1.239842/energy # in nm
    inplaneAng= math.asin(waveL/spacing)*180/numpy.pi
    #print 'expected inplane angle: ', inplaneAng
    return inplaneAng

def d2q(D):
    '''Lattice Distance to q just a 2pi division but maybe useful'''
    return 2*numpy.pi/D
def q2d(q):
    '''Q to lattice, just a division but maybe useful '''    
    return 2*numpy.pi/q

def ang(x,y):
    ''' given the distance x and the y displacement 
    gives the angle T: y=x*sin(T)
    Ex: shadow angle of the beam stop just after the sample  _____|'''
    shad= math.atan(y/x)*180/numpy.pi
    return shad
    
def qang(angle, energy):
    '''It return the Q (nm^-1) exchanged for the couple Angle-Energy '''
    waveL=1.239842/energy # in nm
    Q=2*numpy.pi/waveL    # in nm^(-1)
    Qpar = Q*numpy.sin(angle/(180/numpy.pi)) # in nm^(-1)
    #RealSpacying = 
    return Qpar
    
#def ang2dist(energy,ang):
#    '''Given the angle it recalculates the av distance between the scattering 
#    objects. '''
#    pass
#def q2dist(q):
#    '''for a given q return the distaces between the objects'''
#    pass

def q2ang(energy, qdesired):
    '''Return the scattered angle (deg) for the desired q at the specified energy,
    qdesired couple, q must be in nm^-1'''
    # meant to give q perpendicular
    
    waveL=1.239842/energy
    Q=2*numpy.pi/waveL
    #Qpar = Q*numpy.sin(angle/(180/numpy.pi))
    #q=2Qsin(alpha)
    ang=math.asin(qdesired/(Q))
    
    return numpy.rad2deg(ang)
    
def q2angA(energy, qdesired):
    '''Return the scattered angle (deg) for the desired q at the specified energy,
    qdesired couple, q must be in A^-1'''
    # meant to give q perpendicular
    
    waveL=12.39842/energy
    Q=2*numpy.pi/waveL
    #Qpar = Q*numpy.sin(angle/(180/numpy.pi))
    #q=2Qsin(alpha)
    ang=math.asin(qdesired/(Q))
    
    return numpy.rad2deg(ang)    

    
def braggAngle(energy, planeD):
    '''Given the plane distance (nm) and energy return the n=1 bragg angle. nL=2d*sin(theta)'''
    Lam = en2nm(energy)[0]
    tt2rad = (numpy.arcsin(Lam/(2*planeD)))
    tt2 = numpy.rad2deg(tt2rad)
    return tt2
    
def braggPlane(energy, tt2):
    '''Given the bragg angle and the energy it returns the plane distance for n=1 bragg reflection nL=2d*sin(theta)'''
    Lam=en2nm(energy)[0]
    #print (Lam)
    #print (numpy.deg2rad(angle))
    #print (numpy.sin(numpy.deg2rad(angle)))
    planeD=Lam/(2*(numpy.sin(numpy.deg2rad(tt2))))
    return planeD 
    
def braggEn(planeD, angle):
    '''Given the bragg angle, and plane distance return the energy'''
    #Lam = en2nm(energy)[0]
    Lam  = 2*planeD*numpy.sin(numpy.deg2rad(angle))
    En = nm2en(Lam)
    return En,Lam
    
    pass
def qexc(energy,angle):
    '''Given the energy and reflection agle (2theta) return q in nm-1'''
    waveL = 1.239842/energy
    Q = 2*numpy.pi/waveL
    q = numpy.sin(numpy.deg2rad(angle/2))*Q
        
    return q
def scherr(waveL,fwhm,theta):
    '''Particle size by brutally applying the scherrer formula for fcc.'''
    size = (0.9*waveL)/(numpy.deg2rad(fwhm)*numpy.cos(numpy.deg2rad(theta)))
    return size

def moveMar():
    pass

def plt2Kspace(im,energy,incAng,pixel0x,pixel0y,distance,txt=None):
        '''Egergy in keV   ##### NOT WORKING FOR NOW
        Pixel position of the direct beam
        distance sample to detector in mm
        '''
        LineSize=numpy.shape(im)[0]
        xaxe= (pixel0x-numpy.array(range(0,LineSize,1)))*self.pixSize #direction parallel to the surface
        yaxe= (pixel0y-numpy.array(range(0,LineSize,1)))*self.pixSize
        xang=numpy.arctan(xaxe/distance)
        yang=numpy.arctan(yaxe/distance)
        #print (xang)         
        waveL= 1.239842/energy*1e-9 #1.239842/energy
        Qx=2*numpy.pi/waveL*numpy.cos(incAng)*numpy.sin(xang)
        Qy=2*numpy.pi/waveL*(numpy.sin(yang-incAng)+numpy.sin(incAng))
        #xK = numpy.sin(xang)*2*numpy.pi/waveL ==>oversimplified
        #yK = numpy.sin(yang)*2*numpy.pi/waveL ==>oversimplified
        
        #print(yK)
#        extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ]
#        imshow(hist.T,extent=extent,interpolation='nearest',origin='lower')
#        colorbar()
#        show()
#        extend=[xK[0],xK[-1],yK[0],yK[-1]]
#        plt.imshow(self.im,extend=extend,interpolation="nearest", origin="lower", aspect=1, norm=LogNorm())
        plt.pcolormesh(Qx,Qy,self.im,norm=LogNorm())
        pl.xlabel('Kz (m-1)',fontsize=18)
        pl.ylabel('Kx (m-1)',fontsize=18)
        pl.axis('tight')
        pl.yticks(size=16)
        pl.xticks(size=16)
        #x=max(pixel0x-numpy.array(range(0,LineSize,1)))-500
        x=max(Qx)-0.05*max(Qx)     
        #print(x)
        #y=max(pixel0y-numpy.array(range(0,LineSize,1)))-500
        y=max(Qy)-0.05*max(Qy)
        #print(y)

class mar(object):
    '''It generate the object containing the image data '''
    def __init__(self, filename):
       im=plt.imread(filename)
       self.im = im
       self.pixSize=0.081 #binning 2x2 image 2048x2048 in mm
       self.name=filename
#       self.k = data[:,1]
#       self.l = data[:,2]
#       self.fbulk = data[:,3]
#       self.fsurf = data[:,4]
#       self.fsum = data[:,6]
       
    def pltit(self):
        '''It shows the loaded object as is ''' 
        plt.imshow(self.im,interpolation="nearest", origin="lower", aspect=1, norm=LogNorm())
        plt.annotate(self.name,xy=(50,50), xytext=(.8,0), fontsize=18)
        plt.show()
        
    def plt2Kspace(self,energy,incAng,pixel0x,pixel0y,distance,txt=None):
        '''Egergy in keV
        Pixel position of the direct beam
        distance sample to detector in mm
        '''
        LineSize=numpy.shape(self.im)[0]
        xaxe= (pixel0x-numpy.array(range(0,LineSize,1)))*self.pixSize #direction parallel to the surface
        yaxe= (pixel0y-numpy.array(range(0,LineSize,1)))*self.pixSize
        xang=numpy.arctan(xaxe/distance)
        yang=numpy.arctan(yaxe/distance)
        #print (xang)         
        waveL= 1.239842/energy*1e-9 #1.239842/energy
        Qx=2*numpy.pi/waveL*numpy.cos(incAng)*numpy.sin(xang)
        Qy=2*numpy.pi/waveL*(numpy.sin(yang-incAng)+numpy.sin(incAng))
        #xK = numpy.sin(xang)*2*numpy.pi/waveL ==>oversimplified
        #yK = numpy.sin(yang)*2*numpy.pi/waveL ==>oversimplified
        
        #print(yK)
#        extent = [xedges[0], xedges[-1], yedges[0], yedges[-1] ]
#        imshow(hist.T,extent=extent,interpolation='nearest',origin='lower')
#        colorbar()
#        show()
#        extend=[xK[0],xK[-1],yK[0],yK[-1]]
#        plt.imshow(self.im,extend=extend,interpolation="nearest", origin="lower", aspect=1, norm=LogNorm())
        plt.pcolormesh(Qx,Qy,self.im,norm=LogNorm())
        pl.xlabel('Kz (m-1)',fontsize=18)
        pl.ylabel('Kx (m-1)',fontsize=18)
        pl.axis('tight')
        pl.yticks(size=16)
        pl.xticks(size=16)
        #x=max(pixel0x-numpy.array(range(0,LineSize,1)))-500
        x=max(Qx)-0.05*max(Qx)     
        #print(x)
        #y=max(pixel0y-numpy.array(range(0,LineSize,1)))-500
        y=max(Qy)-0.05*max(Qy)
        #print(y)
        if txt!= None:
            pl.annotate(txt,xy=(0,0), xytext=(2.2e9,1.7e9), fontsize=18)
            
#        plt.legend(loc='lower right')
                
    







    

    