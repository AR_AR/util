# -*- coding: utf-8 -*-
"""
Created on Thu Mar 23 16:44:13 2017

last modified:
Modified again the 09/11/2020

@author: andrea
"""

'''Small scripts to be used to load some of the binoculars outputs'''


import matplotlib.pyplot as plt
import pylab as pl
import os
from natsort import natsorted
import numpy as np
import tables
from tables import NoSuchNodeError
from libtiff import TIFF
### can also use imageio to save tiff####

#from vtk.util import numpy_support
#from tifffile3 import imsave
import util.utilities3 as ut3
from matplotlib.colors import LogNorm


#directory= '/home/andrea/SciProj/MoS2/SXRD/22032017/binoculars_loc/'
#hdf5file1 = 'MoS2_hkl_Au_mask6_crop_296-310_[0.7-1.3,0.7-1.3,-].hdf5'
#hdf5file2 = 'MoS2_hkl_Au_mask6_crop_77-92_[0.7-1.3,0.7-1.3,-].hdf5'
#hdf5file = 'MoS2_hkl_Au_mask6_crop_254-294_[0.9-2.1,m1.1-1.1,-].hdf5'

class hdf5load(object):
    '''Meant to return an object with the data matrix and the axes as attributes
    But better verify than sorry...
    It expects two strings:
        The folder name eg: datdir   = "/home/user/data"
        the file name   eg: filename = "Sample2_hkl_map_1.hdf5"  
        
        syntax example:
        fdata = bu3.hdf5load(datdir, filename)
        
    The object will contains 2 two matrices: ratio between Counts and Contributions original matrices in the hdf5file
    the "data is manipulated to be consistet with the indexing [h, k, l], or [Qx, Qy, Qz].
    it therefore appear rotated compare to dataRaw."
    there are also the 3 (or 2) axes and the original vectors containinig the information to generate the axes.'''
    np.seterr(divide='warn', invalid='warn')
    def __init__(self,directory, hdf5file):
        np.seterr(divide='ignore', invalid='ignore')
        self.data=0
        self.directory =  directory
        self.filename = hdf5file
        self.version = '11/10/2022'
        fullpath = os.path.join(self.directory,self.filename)
        f = tables.open_file(fullpath)
        ff = f.list_nodes('/')[0]
        # works but utterly slow!!!!
        #ct = np.asanyarray( f.list_nodes('/binoculars/')[3],dtype=np.float32) 
        #cont = np.asanyarray( f.list_nodes('/binoculars/')[2],dtype=np.float32)
        self.ct = f.root.binoculars.counts.read() # ~100 times faster 
        self.cont = f.root.binoculars.contributions.read()
        self.dataRaw = self.ct/self.cont    
        hkl = False #True
        QxQy = False #True
        QparQper = False #True
        Qphi = False #True
        Qindex = False #True
        angle = False #True
        for leaf in ff.axes:
                #list.append(attlist,leaf.name) 
                self.__dict__[leaf.name] = leaf[:]
 
                
        if hasattr(self, 'phi'):
            self.data = self.dataRaw
            self.Phi = f.list_nodes('/binoculars/')[0].Phi[:]
            self.Q = f.list_nodes('/binoculars/')[0].Q[:] 
            try:
                self.Qxyz = f.list_nodes('/binoculars/')[0].Qx[:] 
            except:
                pass
            try:
                self.Qxyz = f.list_nodes('/binoculars/')[0].Qy[:] 
            except:
                pass
            try:
                self.Qxyz = f.list_nodes('/binoculars/')[0].Qz[:] 
            except:
                pass
            xaxe = np.linspace(self.Q[1],self.Q[2],1+self.Q[5]-self.Q[4])
            self.Qaxe = xaxe
            yaxe = np.linspace(self.Qxyz[1],self.Qxyz[2],1+self.Qxyz[5]-self.Qxyz[4])
            self.Qxyzaxe = yaxe
            zaxe = np.linspace(self.Phi[1],self.Phi[2],1+self.Phi[5]-self.Phi[4])
            self.Phiaxe = zaxe
            
        if hasattr(self, 'Index'):
            self.data = self.dataRaw
            self.qaxe = np.linspace(self.Q[1],self.Q[2],int(1+self.Q[5]-self.Q[4]))
            indaxe = np.linspace(self.Index[1],self.Index[2],int(1+self.Index[5]-self.Index[4]))
            self.indaxe = indaxe
            # self.index = f.list_nodes('/binoculars/')[0].Index[:]
            # self.Q = f.list_nodes('/binoculars/')[0].Q[:]
        if hasattr(self, 'H'):
#            self.data=np.swapaxes(self.dataRaw,0,2) # not true enymore (13/10/2021)
            self.data = self.dataRaw
            print(type(self.H[5]))
            xaxe = np.linspace(self.H[1],self.H[2],int(1+self.H[5]-self.H[4]))
            self.haxe = xaxe
            yaxe = np.linspace(self.K[1],self.K[2],int(1+self.K[5]-self.K[4]))
            self.kaxe = yaxe
            zaxe = np.linspace(self.L[1],self.L[2],int(1+self.L[5]-self.L[4]))
            self.laxe = zaxe
            # self.H = f.list_nodes('/binoculars/')[0].H[:]
            # self.K = f.list_nodes('/binoculars/')[0].K[:]
            # self.L = f.list_nodes('/binoculars/')[0].L[:]
        if hasattr(self, 'Qz') and hasattr(self, 'Qx')and hasattr(self, 'Qy'):
            try:
                self.data=np.swapaxes(self.dataRaw,0,1) ### to be check
                xaxe = np.linspace(self.X[1],self.X[2],int(1+self.X[5]-self.X[4]))
                self.Qxaxe = xaxe
                yaxe = np.linspace(self.Y[1],self.Y[2],int(1+self.Y[5]-self.Y[4]))
                self.Qyaxe = yaxe
                zaxe = np.linspace(self.Z[1],self.Z[2],int(1+self.Z[5]-self.Z[4]))
                self.Qzaxe = zaxe
            except:
                print('Maybe old file?')
                self.Z = f.list_nodes('/binoculars/')[0].Qz[:]
                zaxe = np.linspace(self.Z[1],self.Z[2],int(1+self.Z[5]-self.Z[4]))
                self.Qzaxe = zaxe
                self.X = f.list_nodes('/binoculars/')[0].Qx[:]
                xaxe = np.linspace(self.X[1],self.X[2],int(1+self.X[5]-self.X[4]))
                self.Qxaxe = xaxe
                self.Y = f.list_nodes('/binoculars/')[0].Qy[:]
                yaxe = np.linspace(self.Y[1],self.Y[2],int(1+self.Y[5]-self.Y[4]))
                self.Qyaxe = yaxe
        if hasattr(self, 'Qper'):
            self.data = self.dataRaw
            xaxe = np.linspace(self.X[1],self.X[2],int(1+self.X[5]-self.X[4]))
            self.Qpar = xaxe
            yaxe = np.linspace(self.Y[1],self.Y[2],int(1+self.Y[5]-self.Y[4]))
            self.Qper = yaxe
            # self.Y = f.list_nodes('/binoculars/')[0].Qper[:]
            # self.X = f.list_nodes('/binoculars/')[0].Qpar[:]
            
            ### for the angles:
        if hasattr(self, 'delta'):
            self.data = self.dataRaw
            self.deltaaxe = np.linspace(self.delta[1],self.delta[2],int(1+self.delta[5]-self.delta[4]))
        if hasattr(self, 'gamma'):
            self.gammaaxe = np.linspace(self.gamma[1],self.gamma[2],int(1+self.gamma[5]-self.gamma[4]))     
        if hasattr(self, 'omega'):
            self.omegaaxe = np.linspace(self.omega[1],self.omega[2],int(1+self.omega[5]-self.omega[4]))   
        if hasattr(self, 'mu'):
            self.muaxe = np.linspace(self.mu[1],self.mu[2],int(1+self.mu[5]-self.mu[4]))

        f.close()
      
        return
        
    def prjaxe(self,axename):
        '''Project on one of the measured axes
        axe is a string: eg 'H'.
        the result is added as attribute .img to the file
        axerange is [0.8, 0.9] and define the positions of the value to be used
        in the array on the desired axe''' 
        #st = axerange[0]
        #nd = axerange[-1]
        try:
            axe = self.__getattribute__(axename)
            axeshape = axe.shape[0]
        except:
            print('Axe name not found')
        
        datashape = self.data.shape
        axenum = datashape.index(axeshape)
        self.img = np.nanmean(self.data,axis = axenum)
        print('check the new attribute  ==> img <==')
        return
    
    def prjaxe_range(self,axename, axerange):
        '''Project on one of the measured axes
        axe is a string: eg 'H'.
        the result is added as attribute .img to the file
        axerange is [0.8, 0.9] and define the positions of the value to be used
        in the array on the desired axe''' 
        #st = axerange[0]
        #nd = axerange[-1]
        try:
            axe = self.__getattribute__(axename)
            axeshape = axe.shape[0]
        except:
            print('Axe name not found')
        
        datashape = self.dataRaw.shape
        axenum = datashape.index(axeshape) # this might be a weak point! but should work most of the times
        st = ut3.find_nearest(axe,axerange[0])[0]
        nd = ut3.find_nearest(axe,axerange[1])[0]
        if axenum == 2:
            datanan=self.dataRaw[:,:,st:nd]
        if axenum == 1:
            datanan=self.dataRaw[:,st:nd,:]
        if axenum == 0:
            datanan=self.dataRaw[st:nd,:,:]
        
        self.imgr = np.nanmean(datanan,axis = axenum)
        print('check the new attribute  ==> imgr <==')
        return
    
    # def prjaxe_range(self, axe,axerange):
    #     '''Project on one of the measured axes
    #     axe is a string: eg 'H'.
    #     the result is added as attribute .img to the file
    #     axerange is [0.8, 0.9] and define the positions of the value to be used
    #     in the array on the desired axe'''        
    #     #datanan = self.data
    #     st = axerange[0]
    #     nd = axerange[-1]
    #     if axe == 'H':
    #         axenum = 2
    #         st = ut3.find_nearest(self.haxe,axerange[0])[0]
    #         nd = ut3.find_nearest(self.haxe,axerange[1])[0]
    #         datanan=self.data[:,:,st:nd]            
    #     if axe == 'K':
    #         axenum = 1
    #         st = ut3.find_nearest(self.kaxe,axerange[0])[0]
    #         nd = ut3.find_nearest(self.kaxe,axerange[1])[0]
    #         datanan=self.data[:,st:nd,:]
    #     if axe == 'L':
    #         axenum = 0
    #         st = ut3.find_nearest(self.laxe,axerange[0])[0]
    #         nd = ut3.find_nearest(self.laxe,axerange[1])[0]
    #         datanan=self.data[st:nd,:,:]
    #     if axe == 'Qz':
    #         axenum = 2
    #         st = ut3.find_nearest(self.Qzaxe,axerange[0])[0]
    #         nd = ut3.find_nearest(self.Qzaxe,axerange[1])[0]
    #         datanan=self.data[:,:,st:nd]
    #     if axe == 'Qy':
    #         axenum = 1
    #         st = ut3.find_nearest(self.Qyaxe,axerange[0])[0]
    #         nd = ut3.find_nearest(self.Qyaxe,axerange[1])[0]
    #         datanan=self.data[st:nd,:,:]
    #     if axe == 'Qx':
    #         axenum = 0
    #         st = ut3.find_nearest(self.Qxaxe,axerange[0])[0]
    #         nd = ut3.find_nearest(self.Qxaxe,axerange[1])[0]
    #         datanan=self.data[:,st:nd,:]
        
    #     self.imgr = np.nanmean(datanan,axis = axenum)
    #     return

    def prjaxes(self, axe1, axe2,range1= None,range2 = None):
        '''Project on one of the measured axes
        axe is a string: eg 'H'.
        the result is added as attribute .img to the file
        axerange is [0.8, 0.9] and define the positions of the value to be used
        in the array on the desired axe''' 
        #st = axerange[0]
        #nd = axerange[-1]
        try:
            axe1 = self.__getattribute__(axe1)
            axe1shape = axe1.shape[0]
        except:
            print('Axe1 name not found')
        
        try:
            axe2 = self.__getattribute__(axe2)
            axe2shape = axe2.shape[0]
        except:
            print('Axe2 name not found')
        
        datashape = self.data.shape
        axe1num = datashape.index(axe1shape)
        axe2num = datashape.index(axe2shape)
        
        if range1 != None:
            st1 = ut3.find_nearest(axe1,range1[0])[0]
            nd1 = ut3.find_nearest(axe1,range1[1])[0]
        if range1 == None:
            st1 = 0
            nd1 = -1
        if range2 != None:
            st2 = ut3.find_nearest(axe2,range2[0])[0]
            nd2 = ut3.find_nearest(axe2,range2[1])[0]
        if range2 == None:
            st2 = 0
            nd2 = -1
        
        if axe1num == 2 and axe2num == 0 :
            datanan=self.data[st2:nd2,:,st1:nd1]
            
        if axe1num == 1 and axe2num == 0 :
            datanan=self.data[st2:nd2,st1:nd1,:]
            
            
        if axe1num == 2 and axe2num == 1:
            datanan=self.data[:,st2:nd2,st1:nd1]
        
        if axe1num == 0 and axe2num == 1 :
            datanan=self.data[st1:nd1,st2:nd2,:]
                    
            
        if axe1num == 0 and axe2num == 2 :
            datanan=self.data[st1:nd1,:,st2:nd2]

        if axe1num == 1 and axe2num == 2 :
            datanan=self.data[:,st1:nd1,st2:nd2]


        if axe2num < axe1num:
            temp = np.nanmean(self.data,axis = axe1num)
            self.int2 = np.nanmean(temp,axis = axe2num)
        if axe2num > axe1num:
            temp = np.nanmean(datanan,axis = axe2num)
            self.int2 = np.nanmean(temp,axis = axe1num)
        print('check the new attribute  ==> int2 <==')
        return

    
    def _hdf2png(self,matrix,plot = 'YES',save = 'NO',axerange = None):
        '''
        Need to be re-implemented: 19/05/2021
        Meant to plot/save a hdf5 map. 
        axerange is a fraction of the range to be used insted of the full extension of it.
        It can also be used only to plot if:
            arg: save = 'NO'  
        It can be used only to plot if:
            arg: plot = 'NO'   '''
        #f = bu3.hdf5load(hdf5dir, el)
        # if axerange == None:
        #     self.prjaxe(axe)
        #     img = self.img
        # if axerange != None:
        #     self.prjaxe_range(axe,axerange)
        #     img = self.imgr
        # if axe == 'H':
        #     axe1 = self.kaxe
        #     axe2 = self.laxe
        #     axe_name1 = 'K (rlu)'
        #     axe_name2 = 'L (rlu)'
        # if axe == 'K':
        #     axe1 = self.haxe
        #     axe2 = self.laxe
        #     axe_name1 = 'H (rlu)'
        #     axe_name2 = 'L (rlu)'
        # if axe == 'L':
        #     axe1 = self.haxe
        #     axe2 = self.kaxe
        #     axe_name1 = 'H (rlu)'
        #     axe_name2 = 'K (rlu)'
        # if axe == 'Qxyz':
        #     axe1 = self.Qaxe
        #     axe2 = self.Phiaxe
        #     axe_name1 = 'Q'
        #     axe_name2 = 'Phi (deg)'
            
                               
        if save !='NO':
            fig = plt.figure(num=None, figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')
            fig.savefig(self.directory+self.filename[:-5]+'_prj'+'.png',format='png', dpi = 600)
            plt.close(fig)
            
        if plot != 'NO':
            #fig = plt.figure(num=None, figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')
            #extent=[f.kaxe.min(),f.kaxe.max(),f.laxe.min(),f.laxe.max()]
            plt.imshow(matrix, cmap='jet', interpolation="nearest", origin="lower", aspect = 'auto',
                       norm=LogNorm(),vmin = 0.01, vmax = 2000,extent=[axe1.min(),axe1.max(),axe2.min(),axe2.max()])
            plt.title(self.filename[10:-5],fontsize = 20)
            plt.xlabel(axe_name1,fontsize = 20)
            plt.ylabel(axe_name2,fontsize = 20)
            plt.tight_layout()
        return

    def _hdf2tiff(self,axe,plot = 'YES',save = 'NO',axerange = None):
        '''Meant to plot/ save a hdf5 map from binoculars 
        Need to be re-implemented: 19/05/2021
        
        axerange is a fraction of the range to be used insted of the full extension of it.'''
        #f = bu3.hdf5load(hdf5dir, el)
        if axerange == None:
            self.prjaxe(axe)
            img = self.img
        if axerange != None:
            self.prjaxe_range(axe,axerange)
            img = self.imgr
        if axe == 'H':
            axe1 = self.kaxe
            axe2 = self.laxe
            axe_name1 = 'K (rlu)'
            axe_name2 = 'L (rlu)'
        if axe == 'K':
            axe1 = self.haxe
            axe2 = self.laxe
            axe_name1 = 'H (rlu)'
            axe_name2 = 'L (rlu)'
        if axe == 'L':
            axe1 = self.haxe
            axe2 = self.kaxe
            axe_name1 = 'H (rlu)'
            axe_name2 = 'K (rlu)'
                               
        if plot == 'YES':
            fig = plt.figure(num=None, figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')
            #extent=[f.kaxe.min(),f.kaxe.max(),f.laxe.min(),f.laxe.max()]
            plt.imshow(img, cmap='jet', interpolation="nearest", origin="lower", aspect = 'auto',
                       norm=LogNorm(),vmin = 0.01, vmax = 2000,extent=[axe1.min(),axe1.max(),axe2.min(),axe2.max()])
            plt.title(self.filename[10:-5],fontsize = 20)
            plt.xlabel(axe_name1,fontsize = 20)
            plt.ylabel(axe_name2,fontsize = 20)
            plt.tight_layout()
        if save =='YES':
            tiff = TIFF.open(self.directory + self.filename[0:-5] + '_' + axe + '.tiff', mode='w')
            tiff.write_image(self.img)
            tiff.close()
        return

    #def intslice(self, haxe, kaxe, Imap, H, K, dH, dK, dbkg):
    def intslice(self,  Imap, H, K, dH, dK, dbkg, haxe='auto', kaxe='auto'):
        '''integrate over a 2D range in H, K and subtract the average background per pixel recalculated
        haxe, kaxe: the axes of the intensity map
        Imap      : is a 2D intesity map in the HK plane
        H K       : are the center of the ROI.
        dH dK     : are the half span of the ROI (use 0.1 to obtain a total width of 0.2)
        dbkg      : is the extension on each the side of the ROI where the bkg is calculated
        ##### ####
        it returns:
        TotBkgInt : The total bkg value estimated over the ROI
        Icorr     : the Intensity subtracted by the backgroung
        RoiInt    : the raw integral of the ROI (not condidering NaN)
        NvoxRoi   : the number of meaningful voxels present in the ROI (no Nan Voxels)
        '''
        if haxe =='auto':
            haxe = self.haxe
        if kaxe =='auto':
            kaxe = self.kaxe
        # retrieve the position in the data matrix of the extension of the ROI
        iHst,value = ut3.find_nearest(haxe,H-dH)
        #print(iHst,value)
        iHnd,value = ut3.find_nearest(haxe,H+dH)
        #print(iHnd,value)
        iKst,value = ut3.find_nearest(kaxe,K-dK)
        #print(iKst,value)
        iKnd,value = ut3.find_nearest(kaxe,K+dK)
        #print(iKnd,value)
        roi = Imap[iHst:iHnd, iKst:iKnd]
        RoiInt = np.nansum(roi) #NaN sum of the ROI
        Nnans = np.count_nonzero(np.isnan(roi)) # Number of NaNs in the roi
        NvoxRoi = roi.shape[0]*roi.shape[1]-Nnans #meaningful voxels number, for Bkg calculation
        
        ###### retrive the background average intensiy per pixel
        #
        #    |﹉﹉﹉|
        #    | [ ] |
        #    |﹎﹎﹎|
        # 
        #bkg1 dH-dbk left side
        iHst,value = ut3.find_nearest(haxe,H-dH-dbkg)
        iHnd,value = ut3.find_nearest(haxe,H-dH)
        iKst,value = ut3.find_nearest(kaxe,K-dK)
        iKnd,value = ut3.find_nearest(kaxe,K+dK)
        bkg1 = Imap[iHst:iHnd, iKst:iKnd]
        bkg1Int = np.nansum(bkg1)
        Nnans_bkg1 = np.count_nonzero(np.isnan(bkg1))
        Nvoxbkg1 = bkg1.shape[0]*bkg1.shape[1]-Nnans_bkg1

        
        #bkg2 dH+dbk right side
        iHst,value = ut3.find_nearest(haxe,H+dH)
        iHnd,value = ut3.find_nearest(haxe,H+dH+dbkg)
        iKst,value = ut3.find_nearest(kaxe,K-dK)
        iKnd,value = ut3.find_nearest(kaxe,K+dK)
        bkg2 = Imap[iHst:iHnd, iKst:iKnd]
        bkg2Int = np.nansum(bkg2)
        Nnans_bkg2 = np.count_nonzero(np.isnan(bkg2))
        Nvoxbkg2 = bkg2.shape[0]*bkg2.shape[1]-Nnans_bkg2
        
        #bkg3 dK+dbk top side
        iHst,value = ut3.find_nearest(haxe,H-dH)
        #print(iHst,value)
        iHnd,value = ut3.find_nearest(haxe,H+dH)
        #print(iHnd,value)
        iKst,value = ut3.find_nearest(kaxe,K+dK)
        #print(iKst,value)
        iKnd,value = ut3.find_nearest(kaxe,K+dK+dbkg)
        bkg3 = Imap[iHst:iHnd, iKst:iKnd]
        bkg3Int = np.nansum(bkg3)
        Nnans_bkg3 = np.count_nonzero(np.isnan(bkg3))
        Nvoxbkg3 = bkg3.shape[0]*bkg3.shape[1]-Nnans_bkg3
        
        #bkg4 dK-dbk bottom side
        iHst,value = ut3.find_nearest(haxe,H-dH)
        #print(iHst,value)
        iHnd,value = ut3.find_nearest(haxe,H+dH)
        #print(iHnd,value)
        iKst,value = ut3.find_nearest(kaxe,K-dK-dbkg)
        #print(iKst,value)
        iKnd,value = ut3.find_nearest(kaxe,K-dK)
        bkg4 = Imap[iHst:iHnd, iKst:iKnd]
        bkg4Int = np.nansum(bkg4)
        Nnans_bkg4 = np.count_nonzero(np.isnan(bkg4))
        Nvoxbkg4 = bkg4.shape[0]*bkg4.shape[1]-Nnans_bkg4
        
        TotBkgVox = Nvoxbkg1 + Nvoxbkg2 + Nvoxbkg3 + Nvoxbkg4
        TotBkgInt = bkg1Int + bkg2Int + bkg3Int + bkg4Int
        #AveBkg = TotBkgInt/TotBkgVox
        #Icorr  = RoiInt-(AveBkg*NvoxRoi)
        Icorr  = RoiInt-(NvoxRoi/TotBkgVox)*TotBkgInt
        # roi - (#roi/#bkg) *bkg?
        
            
        return TotBkgInt, Icorr, RoiInt, NvoxRoi


    def intRod(self, H, K, dH, dK, dL, dbkg, haxe='auto', kaxe='auto', laxe='auto', volume='auto',save_fs = False):
        '''Not too advance metod to numerically integrate the rod volume data.
        h, k define the center position of the signal to be integrated.
        it make use of the above function == > inteslice< == 
        it returns:
        fs          : the structure factor [sqrt(I)/pi]
        LIntervCent : the L value attributed to each L intagration range
        integrals   : the ROI integral - ROI bkgs
        RoiInts     : the ROI integral
        TotBkgInts  : the ROI bkgs
        TotNVoxs    : the meaningful voxel in each ROI 
        
        syntax example:
        fdata = bu3.hdf5load(datdir, filename)
        fs_r, Lsvalue, Lintegrals,RoiInts,TotoBkgInts,TotNVoxs = fdata.intRod( H,K, dH, dK, dL, dbkg)
        fdata.plotfs()
        
        save_fs = False 
        Falseby defalt, if set to True it will save the structure factors and L values
        
        '''
        
        if haxe =='auto':
            haxe = self.haxe
        if kaxe =='auto':
            kaxe = self.kaxe
        if laxe =='auto':
            laxe = self.laxe
        if volume == 'auto':
            volume = self.data
            
        lnd = laxe.max()
        lst = laxe.min()
        
        #Lsteps = round((lnd-lst)/dL)+1 # number of steps
        Lsteps = np.shape(self.laxe)[0]
        
        Lintervals = self.laxe #np.linspace(lst,lnd,Lsteps) # start/end of each interval
        LIntervCent = np.linspace(lst+dL/2,lnd-dL/2,Lsteps-1)  # center position for each interval 
        integrals = np.zeros_like(LIntervCent)
        RoiInts = np.zeros_like(LIntervCent)
        TotBkgInts = np.zeros_like(LIntervCent)
        TotNVoxs = np.zeros_like(LIntervCent)
        for pos,el  in enumerate(LIntervCent):
            #print(Lintervals[pos])
            #print('Nvoxels= ',)
            Li_st, Lval = ut3.find_nearest(laxe, Lintervals[pos])
            Li_nd, Lval = ut3.find_nearest(laxe, Lintervals[pos+1])
            Imap = np.nansum(volume[:,:,Li_st:Li_nd],axis = 2)
            SliceThick=Li_nd-Li_st
                   
            TotBkgInt, Icorr, RoiInt, Nvox = self.intslice( Imap, H, K, dH, dK, dbkg)
            integrals[pos] = Icorr
            RoiInts[pos] = RoiInt
            TotBkgInts[pos] = TotBkgInt
            TotNVoxs[pos] = Nvox*SliceThick
        
            
        fs = np.sqrt(integrals)
        self.fs = fs
        self.L_fs = LIntervCent
        if Lsteps > round((lnd-lst)/dL): #provide the binning at the right spacing.
            print('dL bigger than Binoculars L voxel spacing.')
            L_bin,fs_bin = ut3.binto(LIntervCent, fs, round((lnd-lst)/dL))
            self.fs = fs_bin
            self.L_fs = L_bin
            fs = np.sqrt(fs_bin) #for the output
            LIntervCent = L_bin  #for the output
            
        if save_fs == True:
            self.saveXY()
            
        return fs, LIntervCent, integrals, RoiInts, TotBkgInts, TotNVoxs
    
    def intsliceErr(self,  Imap, ImapCT, H, K, dH, dK, dbkg, haxe='auto', kaxe='auto'):
        '''integrate over a 2D range in H, K and subtract the average background per pixel recalculated
        haxe, kaxe: the axes of the intensity map
        Imap      : is a 2D intesity map in the HK plane
        ImapCT    : the 2D map of the contribution matrix (before dividing for the contributions)
        H K       : are the center of the ROI.
        dH dK     : are the half span of the ROI (use 0.1 to obtain a total width of 0.2)
        dbkg      : is the extension on each the side of the ROI where the bkg is calculated
        ##### ####
        it returns:
        TotBkgInt : The total bkg value estimated over the ROI
        Icorr     : the Intensity subtracted by the backgroung
        RoiInt    : the raw integral of the ROI (not condidering NaN)
        NvoxRoi   : the number of meaningful voxels present in the ROI (no Nan Voxels)
        Ierr      : the error on the integrated intensity, with the contribution from the Bkg
        #### ####
        I = sum(i) 		N = sum(ct) 	
		u(N) = sqrt(N) 	
		u(I)/I = u(N)/N	u(I)=I/sqrt(N)
        
        same for each bkgs, that then must be added to each other under sqrt
            
        '''
        if haxe =='auto':
            haxe = self.haxe
        if kaxe =='auto':
            kaxe = self.kaxe
        # retrieve the position in the data matrix of the extension of the ROI
        iHst,value = ut3.find_nearest(haxe,H-dH)
        #print(iHst,value)
        iHnd,value = ut3.find_nearest(haxe,H+dH)
        #print(iHnd,value)
        iKst,value = ut3.find_nearest(kaxe,K-dK)
        #print(iKst,value)
        iKnd,value = ut3.find_nearest(kaxe,K+dK)
        #print(iKnd,value)
        roi = Imap[iHst:iHnd, iKst:iKnd]
        roiCT = ImapCT[iHst:iHnd, iKst:iKnd]
        RoiInt = np.nansum(roi) #NaN sum of the ROI
        roiErr = RoiInt/np.sqrt(np.nansum(roiCT))
        Nnans = np.count_nonzero(np.isnan(roi)) # Number of NaNs in the roi
        NvoxRoi = roi.shape[0]*roi.shape[1]-Nnans #meaningful voxels number, for Bkg calculation
        
        ###### retrive the background average intensiy per pixel
        #
        #    |﹉﹉﹉|
        #    | [ ] |
        #    |﹎﹎﹎|
        # 
        #bkg1 dH-dbk left side
        iHst,value = ut3.find_nearest(haxe,H-dH-dbkg)
        iHnd,value = ut3.find_nearest(haxe,H-dH)
        iKst,value = ut3.find_nearest(kaxe,K-dK)
        iKnd,value = ut3.find_nearest(kaxe,K+dK)
        bkg1 = Imap[iHst:iHnd, iKst:iKnd]
        bkg1CT = ImapCT[iHst:iHnd, iKst:iKnd]
        bkg1Int = np.nansum(bkg1)
        bkg1Err = bkg1Int/np.sqrt(np.nansum(bkg1CT))
        Nnans_bkg1 = np.count_nonzero(np.isnan(bkg1))
        Nvoxbkg1 = bkg1.shape[0]*bkg1.shape[1]-Nnans_bkg1

        
        #bkg2 dH+dbk right side
        iHst,value = ut3.find_nearest(haxe,H+dH)
        iHnd,value = ut3.find_nearest(haxe,H+dH+dbkg)
        iKst,value = ut3.find_nearest(kaxe,K-dK)
        iKnd,value = ut3.find_nearest(kaxe,K+dK)
        bkg2 = Imap[iHst:iHnd, iKst:iKnd]
        bkg2CT = ImapCT[iHst:iHnd, iKst:iKnd]
        bkg2Int = np.nansum(bkg2)
        bkg2Err = bkg2Int/np.sqrt(np.nansum(bkg2CT))
        
        Nnans_bkg2 = np.count_nonzero(np.isnan(bkg2))
        Nvoxbkg2 = bkg2.shape[0]*bkg2.shape[1]-Nnans_bkg2
        
        #bkg3 dK+dbk top side
        iHst,value = ut3.find_nearest(haxe,H-dH)
        #print(iHst,value)
        iHnd,value = ut3.find_nearest(haxe,H+dH)
        #print(iHnd,value)
        iKst,value = ut3.find_nearest(kaxe,K+dK)
        #print(iKst,value)
        iKnd,value = ut3.find_nearest(kaxe,K+dK+dbkg)
        bkg3 = Imap[iHst:iHnd, iKst:iKnd]
        bkg3Int = np.nansum(bkg3)
        bkg3CT = ImapCT[iHst:iHnd, iKst:iKnd]
        bkg3Err = bkg3Int/np.sqrt(np.nansum(bkg3CT))
        Nnans_bkg3 = np.count_nonzero(np.isnan(bkg3))
        Nvoxbkg3 = bkg3.shape[0]*bkg3.shape[1]-Nnans_bkg3
        
        #bkg4 dK-dbk bottom side
        iHst,value = ut3.find_nearest(haxe,H-dH)
        #print(iHst,value)
        iHnd,value = ut3.find_nearest(haxe,H+dH)
        #print(iHnd,value)
        iKst,value = ut3.find_nearest(kaxe,K-dK-dbkg)
        #print(iKst,value)
        iKnd,value = ut3.find_nearest(kaxe,K-dK)
        bkg4 = Imap[iHst:iHnd, iKst:iKnd]
        bkg4CT = ImapCT[iHst:iHnd, iKst:iKnd]
        bkg4Int = np.nansum(bkg4)
        bkg4Err = bkg4Int/np.sqrt(np.nansum(bkg4CT))
        Nnans_bkg4 = np.count_nonzero(np.isnan(bkg4))
        Nvoxbkg4 = bkg4.shape[0]*bkg4.shape[1]-Nnans_bkg4
        
        # 
        BkgErr = np.sqrt(bkg1Err**2 + bkg2Err**2 + bkg3Err**2 + bkg4Err**2)
        TotBkgVox = Nvoxbkg1 + Nvoxbkg2 + Nvoxbkg3 + Nvoxbkg4
        TotBkgInt = bkg1Int + bkg2Int + bkg3Int + bkg4Int
        #AveBkg = TotBkgInt/TotBkgVox
        #Icorr  = RoiInt-(AveBkg*NvoxRoi)
        Icorr  = RoiInt-(NvoxRoi/TotBkgVox)*TotBkgInt
        Ierr = np.sqrt(roiErr**2 + BkgErr**2)
        # roi - (#roi/#bkg) *bkg?
        
            
        return TotBkgInt, Icorr, RoiInt, NvoxRoi, Ierr
    
    
    def intRodErr(self, H, K, dH, dK, dL, dbkg, haxe='auto', kaxe='auto', laxe='auto', volume='auto',volumeCT='auto',save_fs = False):
        '''Not too advance metod to numerically integrate the rod volume data.
        h, k define the center position of the signal to be integrated.
        it make use of the above function == > inteslice< == 
        it returns:
        fs          : the structure factor [sqrt(I)/pi]
        LIntervCent : the L value attributed to each L intagration range
        integrals   : the ROI integral - ROI bkgs
        RoiInts     : the ROI integral
        TotBkgInts  : the ROI bkgs
        TotNVoxs    : the meaningful voxel in each ROI 
        
        syntax example:
        fdata = bu3.hdf5load(datdir, filename)
        fs_r, Lsvalue, Lintegrals,RoiInts,TotoBkgInts,TotNVoxs = fdata.intRod( H,K, dH, dK, dL, dbkg)
        fdata.plotfs()
        
        save_fs = False 
        Falseby defalt, if set to True it will save the structure factors and L values
        
        '''
        
        if haxe =='auto':
            haxe = self.haxe
        if kaxe =='auto':
            kaxe = self.kaxe
        if laxe =='auto':
            laxe = self.laxe
        if volume == 'auto':
            volume = self.data
        if volumeCT == 'auto':
            volumeCT = self.ct
            
        
            
        lnd = laxe.max()
        lst = laxe.min()
        
        #Lsteps = round((lnd-lst)/dL)+1 # number of steps
        Lsteps = np.shape(self.laxe)[0]
        
        Lintervals = self.laxe #np.linspace(lst,lnd,Lsteps) # start/end of each interval
        LIntervCent = np.linspace(lst+dL/2,lnd-dL/2,Lsteps-1)  # center position for each interval 
        integrals = np.zeros_like(LIntervCent)
        RoiInts = np.zeros_like(LIntervCent)
        TotBkgInts = np.zeros_like(LIntervCent)
        TotNVoxs = np.zeros_like(LIntervCent)
        Ierr = np.zeros_like(LIntervCent)
        for pos,el  in enumerate(LIntervCent):
            #print(Lintervals[pos])
            #print('Nvoxels= ',)
            Li_st, Lval = ut3.find_nearest(laxe, Lintervals[pos])
            Li_nd, Lval = ut3.find_nearest(laxe, Lintervals[pos+1])
            Imap = np.nansum(volume[:,:,Li_st:Li_nd],axis = 2)
            ImapCT = np.nansum(volumeCT[:,:,Li_st:Li_nd],axis = 2)
            SliceThick=Li_nd-Li_st
                   
            TotBkgInt, Icorr, RoiInt, Nvox, Ie = self.intsliceErr( Imap, ImapCT, H, K, dH, dK, dbkg)
            #if np.isnan(Ie):
            #    print('Nan at: ', pos)
            integrals[pos] = Icorr
            RoiInts[pos] = RoiInt
            TotBkgInts[pos] = TotBkgInt
            TotNVoxs[pos] = Nvox*SliceThick
            Ierr[pos] = Ie
        
            
        fs = np.sqrt(integrals)
        fs_err = (1/2)*Ierr
        self.fs = fs
        self.L_fs = LIntervCent
        self.fs_err = fs_err
        if Lsteps > round((lnd-lst)/dL): #provide the binning at the right spacing.
            print('dL bigger than Binoculars L voxel spacing.')
            L_bin,fs_bin = ut3.binto(LIntervCent, fs, round((lnd-lst)/dL))
            # missing function to obtain error from binning
            L_bin,fs_bin, fs_err  = ut3.bintoErr(LIntervCent, fs, fs_err,round((lnd-lst)/dL))
            self.fs = fs_bin
            self.L_fs = L_bin
            self.fs_err = fs_err
            fs = fs_bin #for the output
            LIntervCent = L_bin  #for the output
            
        if save_fs == True:
            self.saveXY()
            
        return fs, LIntervCent, fs_err, integrals, RoiInts, TotBkgInts, TotNVoxs    
    
    
    
    
    def saveXY(self, xy = ['L_fs', 'fs','fs_err']):
        '''It saves a txt file with the X Y variables listed ['x','y'].
        
        it expect a list of strings. 
        It dos not export images nor  x y arributes of inconsistent lenght
        '''
        filesave = self.filename[:-5] + '_extr.txt'
        longname = os.path.join(self.directory,filesave)
    
        #x = np.empty(self._npts)
        #y = np.empty(self._npts)
        
        x = self.__getattribute__(xy[0])
        y = self.__getattribute__(xy[1])
        
        try:
            err = self.__getattribute__(xy[2])
            err_out = err.round(5)
        except: # if there is no fs_err return nans for that column.
            err_out = np.zeros_like(x)*np.nan
            
            

        #print (y.shape)
        yout = y.round(5)
        xout = x.round(5)
        if len(np.shape(x))<2 and len(np.shape(y))<2: 
            if len(x) == len(y):
                outdata = np.c_[xout,yout,err_out]          
                headerline = '   '.join(xy)     # generating the header         
                np.savetxt(longname, outdata, delimiter = '\t',fmt = '%.5e' ,header=headerline)
            else:
                print('X, Y must have the same leght')
        else:
            print('Both X and Y must be 1D.')
    
    
    def sliceInterp2D():
        '''It interpolated before integrating.
        see file : 
            http://localhost:8888/notebooks/Documents/PhD_David/data/20211017_Pt100/Binoculars/RodIntegrateTest.ipynb#
            for testsf'''
        return

    def plotfs(self,color='g',label='auto',title = 'auto'):
        '''meant to plot the structure factor (fs) calculated by the function above '''
        if hasattr(self, 'fs'):
            if label != 'auto':
                label = label
            else:
                label = 'fs'
            if title != 'auto':
                title = title
            else:
                title = self.filename
            
            if hasattr(self,'fs_err'): #plot the error bar if exists
                plt.errorbar(self.L_fs, self.fs, yerr=self.fs_err, ecolor='r',fmt='.g',label=label)
                
            else:    
                plt.plot(self.L_fs, self.fs, color,fmt = '+g', label=label)
            plt.title(title)
            plt.legend()
            plt.semilogy()
            plt.xlabel('L r.l.u.')
        else:
            print('== > Need to run the intRod function first < ==')


    def sliceplot(self, Lrange):
        '''Plots a slice in L of the desired thickness
        Lrange is [0.05, 0.15]
        It also add the calculated intensity map as self.Imap'''
        h = self.haxe
        k = self.kaxe
        laxe = self.laxe
        volume = self.data
        Li_st, Lval = ut3.find_nearest(laxe, Lrange[0])
        Li_nd, Lval = ut3.find_nearest(laxe, Lrange[1])
        Imap = np.nansum(volume[:,:,Li_st:Li_nd],axis = 2)
        Imap90=np.rot90(Imap) # to have H on the x axe
        plt.pcolormesh(h,k,Imap90,shading='auto',cmap=plt.cm.jet,norm=LogNorm())
        title = self.filename + ' L range: ' + str(Lrange[0]) + ' to ' + str(Lrange[1])
        plt.title(title)
        plt.ylabel('K, r.l.u.')
        plt.xlabel('H, r.l.u.')
        plt.colorbar()
        self.Imap90=Imap90
        return Imap90


##### general functions below here, not applied to the class


def removeNan(x,y):
    '''it checks if any of the two vectors contains nan and remove them therein 
    and from its pair in the other vector'''
    if np.shape(y) == np.shape(x):
        ynan = np.argwhere(np.isnan(y))
        yf = np.delete(y, ynan)
        xf = np.delete(x, ynan)
        xnan = np.argwhere(np.isnan(xf))
        xff = np.delete(xf, xnan)
        yff = np.delete(yf, xnan)
    return xff, yff

def rodload(directory, rodFile):
    '''It reads the integreted rod file output of Binoculars-fitaid '''
    data=np.genfromtxt(directory + rodFile,dtype= 'float', delimiter='')
    return data

def txt2dat(directoryIN, filelist, rodlist,directoryOUT,fileout , LbraggList = []):
    '''It takes 3 entry:
       the data directory,
       the filenames list of the Binoculars-fitaid integrated rods,
       rodlist is a list: [[h,k],[h,k]].... eg:[[-1,1],[1,0]]
       Lbragg parameter for the roughthness calculations, depends on the ctr
       The output directory
       the output file name. eg: filemane.dat
       It transform the data in a dat file that can be imported from ROD'''
    #print (np.shape(rodlist))
    #print (np.shape(filelist))
    if int(np.shape(rodlist)[0]) ==  int(np.shape(filelist)[0]):
        #print ('yes')
        g = open(directoryOUT+fileout, 'w+')
        g.write('Generated from Binoculars Files' + '\n')
        space = '    '
        if LbraggList == []:
            for n,el in enumerate(filelist):
                LbraggList.append(space)
        for n,el in enumerate(filelist):
            data=rodload(directoryIN, str(el))
            l,sf=removeNan(data[:,0],data[:,1])
            h = rodlist[n][0]
            k = rodlist[n][1]
            lb = LbraggList[n]
            for j in np.arange(np.shape(l)[0]):
                g.write(str(h) + space + str(k) + space + '%.3f'%l[j] + space + '%.3f'%sf[j] + space + '%.3f'%(sf[j]*0.09) + space + '%.3f'%lb+ '\n')
                #g.write(str(h) + space + str(k) + space + '\n')
                #print(l[j])
    return

              
    
def rodplot(directory, rodFile, color, scaleMax = 'NO',label = None, scFactor = 1):
    '''It plots the integartions obtained/exported from binoculars-fitaid
    rodfile'''
    #plt.figure(num=None, figsize=(12, 7), dpi=80, facecolor='w', edgecolor='k')
    data=np.genfromtxt(directory + rodFile,dtype= 'float', delimiter='')#, skip_header=3)
    L0 = data[:,0]
    Y0 = np.asanyarray(data[:,1])
    L,Y = removeNan(L0,Y0)
    #L = L0
    #Y = Y0
    Int = Y *scFactor
    if scaleMax != 'NO':
        maxpos = np.nanargmax(Y)
        print((maxpos, Y[maxpos]))
        Int = Y/Y[maxpos]       
    #data[:,1]=data[:,1]-min(data[:,1])
    if not label:
        #plt.plot(L,Int,color,linewidth=2, label=rodFile[:-4])
        plt.plot(L,Int,color,marker='+',linewidth=0, label=rodFile[:-4])
    if (label !=None and label !='No'):
        #plt.plot(L,Int,color,linewidth=2, label=label)
        plt.plot(L,Int,color,marker='+',linewidth=0, label=label)
    if label =='No':
        #plt.plot(L,Int,color,linewidth=2)
        plt.plot(L,Int,color,marker='+',linewidth=0)
    
    pl.semilogy()
    pl.xlim(min(L),max(L))   
    pl.ylim(0, max(Y))
    if (label !=None and label !='No'):
        plt.legend(loc=1,fontsize=10)
    plt.grid(True)
    plt.show()     
    
    
def rodplot_diff(directory, rodFile, bkgFile, color,plotOrig = 'NO'):
    '''It plots the integrations obtained/exported from binoculars-fitaid
    rodfile'''
    #plt.figure(num=None, figsize=(12, 7), dpi=80, facecolor='w', edgecolor='k')
    data1 = rodload(directory, rodFile)
    data2 = rodload(directory, bkgFile)
    #data1=np.genfromtxt(directory + rodFile,dtype= 'float', delimiter='')#, skip_header=3)
    L1 = data1[:,0]
    Y1 = np.nan_to_num(np.asanyarray(data1[:,1]))
    #L1,Y1 = removeNan(L0,Y0)
    L2 = data2[:,0]
    Y2 = np.nan_to_num(np.asanyarray(data2[:,1]))
    #L2,Y2 = removeNan(L0,Y0)
    #L = L0
    #Y = Y0
    if plotOrig != 'NO':
        plt.plot(L1, Y1,'k',marker='+',linewidth=0, label = 'Rod')
        plt.plot(L2,Y2,'g',marker='+',linewidth=0, label = 'bkb')
    
    plt.plot(L1,Y1-Y2,color,marker='+',linewidth=0, label = 'Rod-bkg')
    
    pl.semilogy()
    pl.xlim(min(L1),max(L1))   
    pl.ylim(0, max(Y1))
    plt.legend()
    plt.semilogy()
    plt.grid(True)
    plt.show()



        
            

